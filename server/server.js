var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors');

var contract = new (require('./ContractInterface.js'))();

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use(cors())

var port = process.env.PORT || 8080;

var router = express.Router();

app.post('/buy', function(req, res){
    console.log(JSON.stringify(req.body));
    const address = req.body.address;
    const amount = Number(req.body.amount);
    console.log("buy " + amount + " from "+address);
    contract.buy(address, amount, ()=>{
        res.json({ message: 'bought'});
    },()=>{
        res.json({ message: 'failed to buy'});
    });
    
})

app.post('/sell', function(req, res){
    const address = req.query.address;
    const amount = Number(req.query.amount);
    console.log("sell " + amount + " to "+address);
    contract.sell(address, amount, ()=>{
        res.json({ message: 'sold'});
    },()=>{
        res.json({ message: 'failed to sell'});
    });
})

app.get('/balance', function(req, res){
    const address = req.query.address;
    console.log("checking balance of  "+address);
    if(address != null){
        contract.getBalance(address,(bal)=>{
            res.json({ balance: bal})
        })
    }else{
        res.json({ error: "Address required!"})
    }
})

app.listen(port);

console.log('Server running on ' + port);
