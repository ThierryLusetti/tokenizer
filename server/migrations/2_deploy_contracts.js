var SimpleStorage = artifacts.require("./SimpleStorage.sol");
var AAPLToken = artifacts.require("./AAPLToken.sol");

module.exports = function(deployer) {
  deployer.deploy(SimpleStorage);
  deployer.deploy(AAPLToken)
};
