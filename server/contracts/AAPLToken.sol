
pragma solidity ^0.4.24;

import "./MintableBurnableToken.sol";

contract AAPLToken is MintableBurnableToken {

    string public constant name = "AppleToken";
    string public constant symbol = "AAPLT";
    uint8 public constant decimals = 18;
    uint256 public constant INITIAL_SUPPLY = 0 * (10 ** uint256(decimals)); //kept for reference


    constructor() public {
        totalSupply_ = INITIAL_SUPPLY;
        balances[msg.sender] = INITIAL_SUPPLY;
        emit Transfer(address(0), msg.sender, INITIAL_SUPPLY);
    }

}