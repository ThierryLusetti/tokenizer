//test

const AAPLToken = require('./build/contracts/AAPLToken.json')
//end test

class ContractInterface{
    constructor(){
        var Web3 = require('web3');
        this.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));//might not work on prod
        const privateKey = '6fa58177f3d1875e0310f6a7ff0e87de70dc8f760b6d9b5fc1be41c55a036c51'; // UPDATE EVERYTIME YOU DEPLOY NEW BLOCKCHAIN "testrpc"
        const account = this.web3.eth.accounts.privateKeyToAccount('0x' + privateKey);
        console.log("account:"+account);
        this.web3.eth.accounts.wallet.add(account);
        this.web3.eth.defaultAccount = account.address;

        const contract = require('truffle-contract')
        const aaplContract = contract(AAPLToken)
        aaplContract.setProvider(this.web3.currentProvider)
        //dirty hack for web3@1.0.0 support for localhost testrpc, see https://github.com/trufflesuite/truffle-contract/issues/56#issuecomment-331084530
        if (typeof aaplContract.currentProvider.sendAsync !== "function") {
            aaplContract.currentProvider.sendAsync = function() {
                return aaplContract.currentProvider.send.apply(
                    aaplContract.currentProvider, arguments
                );
            };
        }
        this.aaplContract  = aaplContract;
    };
    buy(address, amount, success, failure){
        this.aaplContract.deployed().then((instance) => {
                console.log("buying...");
                instance.mint(address, amount, {
                    from: this.web3.eth.defaultAccount
                }).then((result) => {
                    console.log("Result:" + JSON.stringify(result));
                    success();
                }).catch((error)=>{
                    console.log("Error:"+JSON.stringify(error));
                    failure();
                });
          })
    }
    sell(address, amount, success, failure){
        this.aaplContract.deployed().then((instance) => {
                console.log("selling...");
                instance.burnFrom(address, amount, {
                    from: this.web3.eth.defaultAccount
                }).then((result) => {
                    console.log("Result:" + JSON.stringify(result));
                    success();
                }).catch((error)=>{
                    console.log("Error:"+JSON.stringify(error));
                    failure();
                });
          })
    }
    getBalance(address,success){
        this.aaplContract.deployed().then((instance) => {
            instance.balanceOf(address)
            .then((result) => {
                console.log("Result:" + JSON.stringify(result));
                success(result);
            });//.call(this.web3.eth.defaultAccount)
        })
    }
}
module.exports =  ContractInterface;