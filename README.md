GLOBAL SETUP (as admin):
npm install -g ganache-cli
npm install -g truffle
npm install -g --unsafe-perm  websocket


SETUP:

//install dependencies

npm install

//launch local ethereum test network

testrpc
//or with
ganache-cli

//compile contracts

truffle compile

//deploy contracts to test net

truffle migrate --development --reset

//launch node server

node server.js

