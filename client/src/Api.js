import openSocket from 'socket.io-client'
const socket = openSocket('https://ws-api.iextrading.com/1.0/last')

function getStockValues(cb){
    socket.on('message', stock => cb(null, stock))
    
    // Connect to the channel
    socket.on('connect', () => {
      // Subscribe to topics (i.e. appl,fb,aig+)
      socket.emit('subscribe', 'googl')
    })
    
    // Disconnect from the channel
    socket.on('disconnect', () => console.log('Disconnected.'))
}

export { getStockValues }