import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { getStockValues } from './Api';
import getWeb3 from './utils/getWeb3'
import AAPLToken from "./contracts/AAPLToken.json"
import axios from "axios"


class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      web3: null,
      accounts: null,
      contract: null,
      sellDialog:false
    }

    getStockValues((err,stock) => {
      let obj = JSON.parse(stock)
      this.setState({
        obj
      })
    })

    this.state = {
      stock: 'no stock yet',
      obj: ''
    }
  }
  
  instantiateContract() {
    console.log("1")
    const contract = require('truffle-contract')
    console.log("2")
    const aaplContract = contract(AAPLToken)
    console.log("3")
    aaplContract.setProvider(this.state.web3.currentProvider)
    console.log("4")

    var contractInstance;
    console.log("5")
    this.state.web3.eth.getAccounts((error, accounts) => {
      console.log("6")
        this.setState({
            accounts: accounts
        });
        console.log("6.5")
        aaplContract.deployed().then((instance) => {
          contractInstance = instance
          console.log("7")
            this.setState({
                contract: contractInstance
            });
          console.log(contract);
          alert("Contract found!");
        })
    })
}

componentWillMount() {
	getWeb3
		.then(results => {
			this.setState({
				web3: results.web3
			})
			this.instantiateContract()
		})
		.catch(() => {
			console.log('Error finding web3. :(')
		})
}
  
  componentDidMount() {

  }
  
  // Asynchronous buy function, needs to have request
  async buy(e) {
    e.preventDefault();
    const btcAddress = "0x7f23de5e98d242ba5217d8ab6fbf47733cbaf063"; //TODO remeber to update everytime u restart testrpc
    const purchaseAmount = 1; //TODO take it from an input field
    const res = await axios.post('http://localhost:8080/buy', {
      address: btcAddress,
      amount: purchaseAmount
    });
    console.log(res);
    alert(JSON.stringify(res));
  }

  confirmSell(){
    const r =  window.confirm("Allow sell?"); 
    const allowedAddress = "0xa5ec6a333c5858f4127d3a4d5870fa0632bd2df3"; //TODO remeber to update everytime u restart testrpc
    if(r == true){ 
      console.log("spender address:"+allowedAddress);
      this.state.contract.approve(allowedAddress, 1, {
          from: this.state.accounts[0]
      }).then((result) => {
          console.log("Result:" + JSON.stringify(result));
          fetch('http://localhost:8080/sell?address='+this.state.accounts[0]+'&amount=1', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': 'http://localhost:8080',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers': 'Content-Type'
            }
        }).then((response) => {
          if (response.status >= 400) {
            alert("Error selling!");
          }else{
            response.json().then((results)=>{
              alert(results.message);
            });
            
          }
          this.unallow(allowedAddress);
        }).catch((ex) => {
            alert("Error selling!");
            this.unallow(allowedAddress);
        })
      }).catch((error)=>{
          console.log("Error:"+JSON.stringify(error));
          alert("Failed to allow selling!")
      });
      this.state.contract.approve;
    }else{
      //do nothing
    }
  }

  unallow(address){
    this.state.contract.approve(address, 1, {
      from: this.state.accounts[0]
    }).then((result) => {
      alert("Allowance cleared.");
    });
  }

  render() {
    
    return (
      <div className="App">
      
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Asset Tokenizer</h1>
        </header>
        <p className="App-intro">Latest Google Stock Price: {this.state.obj.price}</p>
        <button onClick={this.buy}> Buy </button>
        <button onClick={this.confirmSell.bind(this)}> Sell </button>
      </div>
    );
  }
}

export default App;
